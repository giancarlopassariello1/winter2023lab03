import java.util.Scanner;
public class ApplianceStore {
	
	public static void main (String[] args) {
		Stove[] stoves = new Stove[4];
		Scanner r = new Scanner(System.in);
		for (int i = 0; i < 4; i++) {
			stoves[i] = new Stove();
			System.out.println("What price is your stove ?");
			stoves[i].price = r.nextDouble();
			
			System.out.println("What brand is your stove ?");
			stoves[i].brand = r.next();
			
			System.out.println("What is your stove type ?");
			stoves[i].stoveType = r.next();
		}
		
		System.out.println("Stove price : "+stoves[3].price);
		System.out.println("Stove brand : "+stoves[3].brand);
		System.out.println("Stove type : "+stoves[3].stoveType);
		
		stoves[0].printBrand();
		stoves[0].recommendedPrice();
	}
}

