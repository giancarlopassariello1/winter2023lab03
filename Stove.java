public class Stove {
	public double price;
	public String brand;
	public String stoveType;
	
	public void printBrand() {
		System.out.println("Your stove's brand is : "+this.brand);
	}
	
	public void recommendedPrice() {
		if (this.price <= 800) {
			System.out.println("Your stove's price below the average price!");
		} else if (this.price > 800 && this.price < 2000) {
			System.out.println("Your stove's price is roughly the average price!"); }
		else {
			System.out.println("Your stove is overpriced!");
		}
			
	}
}

